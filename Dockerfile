FROM openjdk:8-jdk-alpine
VOLUME /tmp
ENV PORT 8080
EXPOSE $PORT
COPY target/winFramework-*-SNAPSHOT.jar  app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=${PORT}","-jar","/app.jar"]